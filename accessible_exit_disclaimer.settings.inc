<?php

/**
 * @file
 * Functionality for Accessible Exit Disclaimer management settings.
 */

/**
 * Settings form as implemented by hook_menu.
 */
function accessible_exit_disclaimer_settings($form, &$form_state) {

  // Add global setting for the site name.
  $form['accessible_exit_disclaimer_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Disclaimer title'),
    '#description' => t('Text to be displayed in the title of the modal window.'),
    '#default_value' => variable_get('accessible_exit_disclaimer_title', 'Leaving Site'),
  );

  $message_wysiwyg = variable_get('accessible_exit_disclaimer_message', array('value' => 'You are about to leave this site to go to an external site. Visit <a href="http://example.com" title="Read external site policy">example.com</a> to learn more.', 'format' => 'full_html'));

  // Add global setting for the disclaimer message.
  $form['accessible_exit_disclaimer_message'] = array(
    '#type' => 'text_format',
    '#title' => t('Disclaimer message'),
    '#default_value' => $message_wysiwyg['value'],
    '#format' => $message_wysiwyg['format'],
  );

  $form['accessible_exit_disclaimer_urls'] = [
    '#type' => 'textarea',
    '#title' => t('Trusted sites (Enter one URL per line)'),
    '#description' => t('<p>URLs to be included as Trusted Sites.  These URLs will not display a modal window when clicked.  Enter one URL per line.</p><p><strong>Accepted URL patterns:</strong></p><ul><li>https://www.example.com</li><li>www.example.com</li><li>subdomain.example.com</li><li>*.com</li><li>example.*</li><li>mailto:example@example.com</li></ul>'),
    '#rows' => 10,
    '#default_value' => variable_get('accessible_exit_disclaimer_urls', '*.gov'),
  ];

  // Add 'CSS and Layout' collapsed fieldset.
  $form['accessible_exit_disclaimer_css_and_layout'] = array(
    '#type' => 'fieldset',
    '#title' => t('CSS and Layout'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  // Add global theme setting for a custom CSS file.
  $form['accessible_exit_disclaimer_css_and_layout']['accessible_exit_disclaimer_custom_css'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to Custom CSS File'),
    '#description' => t('To override the default Accessible Exit Disclaimer CSS layout, enter the path to your custom CSS file.  It should be a relative path from the root of your Drupal install (e.g. sites/all/themes/example/custom.css).'),
    '#default_value' => variable_get('accessible_exit_disclaimer_custom_css', ''),
  );

  // Add 'Exit Button' collapsed fieldset.
  $form['accessible_exit_disclaimer_css_and_layout']['accessible_exit_disclaimer_exit_button'] = array(
    '#type' => 'fieldset',
    '#title' => t('Exit Button'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  // Add global theme setting for a custom exit button image.
  $form['accessible_exit_disclaimer_css_and_layout']['accessible_exit_disclaimer_exit_button']['accessible_exit_disclaimer_exit_button_image'] = array(
    '#type' => 'textfield',
    '#title' => t('Exit Button Image URL Path'),
    '#description' => t('To override the default Accessible Exit Disclaimer exit button image, enter the path to your desired image.  PNG is the preferred format.  It should be a relative path from the root of your Drupal install (e.g. sites/all/themes/example/images/exit.png).'),
    '#default_value' => variable_get('accessible_exit_disclaimer_exit_button_image', drupal_get_path('module', 'accessible_exit_disclaimer') . "/img/close_modal.png"),
  );

  // Add global theme setting for exit button image's alt attribute.
  $form['accessible_exit_disclaimer_css_and_layout']['accessible_exit_disclaimer_exit_button']['accessible_exit_disclaimer_exit_button_alt'] = array(
    '#type' => 'textfield',
    '#title' => t('Exit Button Image Alt Attribute'),
    '#description' => t('Enter the alt attribute for the exit button image.'),
    '#default_value' => variable_get('accessible_exit_disclaimer_exit_button_alt', 'Close the modal window'),
  );

  // Add 'Footer Buttons' collapsed fieldset.
  $form['accessible_exit_disclaimer_css_and_layout']['accessible_exit_disclaimer_bottom_buttons'] = array(
    '#type' => 'fieldset',
    '#title' => t('Footer Buttons'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  // Add global theme setting for continue button text.
  $form['accessible_exit_disclaimer_css_and_layout']['accessible_exit_disclaimer_bottom_buttons']['accessible_exit_disclaimer_continue_button_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Change "Continue Button" Text'),
    '#description' => t('Enter the text you want to display inside the continue button. This setting replaces "Continue" text and value.'),
    '#default_value' => variable_get('accessible_exit_disclaimer_continue_button_text', 'Continue'),
  );

  // Add global theme setting for stay here button text.
  $form['accessible_exit_disclaimer_css_and_layout']['accessible_exit_disclaimer_bottom_buttons']['accessible_exit_disclaimer_stay_here_button_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Change "Stay Here" Button Text'),
    '#description' => t('Enter the text you want to display inside the Stay Here button. This setting replaces "Stay Here" text and value attribute.'),
    '#default_value' => variable_get('accessible_exit_disclaimer_stay_here_button_text', 'Stay Here'),
  );

  // Add 'Advanced CSS and Layout' fieldset.
  $form['accessible_exit_disclaimer_css_and_layout']['accessible_exit_disclaimer_advanced_css_and_layout'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced CSS and Layout'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  // Add global theme setting for assigning the html element to bind the modal
  // window.
  $form['accessible_exit_disclaimer_css_and_layout']['accessible_exit_disclaimer_advanced_css_and_layout']['accessible_exit_disclaimer_binding_html_element'] = array(
    '#type' => 'textfield',
    '#title' => t('Binding HTML Class or ID'),
    '#description' => t('Assign HTML Class or ID used to open and close the modal window.  It is recommended the class or ID be attached to the container element.'),
    '#default_value' => variable_get('accessible_exit_disclaimer_binding_html_element', '.page'),
  );

  // Add global theme setting for assigning the html element the JavaScript will
  // look for links to check if trusted or not.
  $form['accessible_exit_disclaimer_css_and_layout']['accessible_exit_disclaimer_advanced_css_and_layout']['accessible_exit_disclaimer_html_element_links'] = array(
    '#type' => 'textfield',
    '#title' => t('HTML Element for Link Lookup'),
    '#description' => t('Assign HTML class, ID, or element used by script to verify the clicked link href attribute is trusted.  Only anchor links within the class, ID, or element will be checked if trusted or not.  Classes should begin with "." and IDs should begin with "#".'),
    '#default_value' => variable_get('accessible_exit_disclaimer_html_element_links', 'body'),
  );

  // Add '508 Settings' collapsed fieldset.
  $form['accessible_exit_disclaimer_508_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('508 Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  // Add global theme setting for instructions to be read by screen
  // reader users.  Describes what the modal window is doing.
  $form['accessible_exit_disclaimer_508_settings']['accessible_exit_disclaimer_screen_reader_message'] = array(
    '#type' => 'textarea',
    '#title' => t('Screen Reader Message'),
    '#description' => t('Message for screen reader users to describe what the popup is designed to do.'),
    '#default_value' => variable_get('accessible_exit_disclaimer_screen_reader_message', 'Beginning of dialog window. The escape key, cancel button, and close button will cancel and close the window. Select continue to continue to the clicked link.'),
  );

  // Add 'Advanced' collapsed fieldset.
  $form['accessible_exit_disclaimer_advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  // Add global theme setting for the option to click outside of modal window
  // to close it.
  $form['accessible_exit_disclaimer_advanced']['accessible_exit_disclaimer_click_outside_close'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Click Outside Modal Window to Close'),
    '#description' => t('Enables users to close the modal window by clicking outside  modal window.  Not accessible to keyboard users.'),
    '#default_value' => variable_get('accessible_exit_disclaimer_click_outside_close', 1),
  );

  // Add global setting for the option of a redirect countdown script.
  $form['accessible_exit_disclaimer_advanced']['accessible_exit_disclaimer_countdown_redirect'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Countdown Redirect'),
    '#description' => t('If enabled, the user will be redirected to the selected link location after the countdown has expired.  Note:  When selecting this option, keep in mind the option is not 508 compliant.'),
    '#default_value' => variable_get('accessible_exit_disclaimer_countdown_redirect', 0),
  );

  $form['accessible_exit_disclaimer_advanced']['time_container'] = array(
    '#type' => 'container',
    '#states' => array(
      'visible' => array(
        ':input[name="accessible_exit_disclaimer_countdown_redirect"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['accessible_exit_disclaimer_advanced']['time_container']['accessible_exit_disclaimer_countdown_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Time until redirect (in seconds)'),
    '#default_value' => variable_get('accessible_exit_disclaimer_countdown_time', 15),
  );

  // Custom validation to make sure the user is entering values.
  $form['#validate'][] = 'accessible_exit_disclaimer_settings_validate';

  return system_settings_form($form);
}

/**
 * Custom validation for the disclaimer settings form.
 */
function accessible_exit_disclaimer_settings_validate($form, &$form_state) {
  $seconds = $form_state['values']['accessible_exit_disclaimer_countdown_time'];
  $css_path = $form_state['values']['accessible_exit_disclaimer_custom_css'];
  $check_extension = substr($css_path, -4);

  if (!empty($css_path) && strlen($css_path) > 4 && $check_extension !== '.css') {
    form_set_error('accessible_exit_disclaimer_custom_css', t('Please enter a valid CSS url.  Path must end with .css.'));
  }

  // Check to see if it is a number and if it is less than a minute.
  if (!is_numeric($seconds) || $seconds > 60) {
    form_set_error('accessible_exit_disclaimer_countdown_time', t('You must enter a number from 0 to 60.'));
  }

}
