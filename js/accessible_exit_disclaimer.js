/**
 * @file
 * Provides functionality for the Accessible Exit Disclaimer and creates the trusted site array for the Disclaimer.
 */
(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.accessible_exit_disclaimer = {
    attach: function (context, settings) {
      if (Drupal.settings.accessible_exit_disclaimer) {

        $(document).ready(function () {
          var $elementsToHide = 'body > div, body > section, body > main';
          $($elementsToHide).not('#accessible-exit-disclaimer, #accessible-exit-disclaimer-overlay').attr('aria-hidden', 'false'); // Mark all child elements of body as visible.
        });

        // Search for focusable items and store them
        var $focusedElementBeforeModal;
        var $focusableElementsString = 'a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, *[tabindex], *[contenteditable]';

        // Store values from module setting's JS array into variables.
        var $trusted_sites = settings.accessible_exit_disclaimer.trusted_sites;
        var $modalBindingElement = settings.accessible_exit_disclaimer.bindingElement;
        var $anchorLookup = settings.accessible_exit_disclaimer.anchor_lookup_element;
        var $modalCloseOutsideWindow = settings.accessible_exit_disclaimer.click_outside_close;
        var $modalCountdownRedirect = settings.accessible_exit_disclaimer.countdown_redirect;
        var $modalCountdownTime = settings.accessible_exit_disclaimer.countdown_time;

        /**
         * Parses a URL using regex and returns values to the host, tld array keys.
         *
         * @param {string} url - URL to be parsed into parts.
         * @return {object} - Return hostname and TLD from regex matches.
        */
        var parseURL = function (url) {
          var $subdomain;
          var $matches;

          if (!url || url === '#' || url.indexOf('#') == 0) {
            return false;
          }

          // Check for data URIs; See, e.g.: https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/Data_URIs
          // Highcharts data export capabilities leverage this type of URI.
          if (url.indexOf('data:') == 0) {
            return false;
          }

          var $regex = /(?:([^\:]*)\:\/\/)?(?:([^\:\@]*)(?:\:([^\@]*))?\@)?(?:([^\/\:]*)\.(?=[^\.\/\:]*\.[^\.\/\:]*))?([^\.\/\:]*)(?:\.([^\/\.\:]*))?(?:\:([0-9]*))?(\/[^\?#]*(?=.*?\/)\/)?([^\?#]*)?(?:\?([^#]*))?(?:#(.*))?/;

          $matches = $regex.exec(url);

          // Check for javascript commands in href attribute.  Mainly for ckeditor.
          if ($matches[5] === 'javascript') {
            return false;
          }

          // Check for subdomains.  If true, combine subdomain and domain.
          if (typeof $matches[4] !== 'undefined' && $matches[4] !== '' && $matches[4] !== 'www') {
            $subdomain = $matches[4] + '.' + $matches[5];
          }
          else {
            $subdomain = $matches[5];
          }

          // Check for relative links.
          if ($matches[5] === '' && $matches[8] !== '') {
            var $relative = $regex.exec(window.location.hostname);

            if (typeof $relative[4] !== 'undefined' && $relative[4] !== '' && $relative[4] !== 'www') {
              $subdomain = $relative[4] + '.' + $relative[5];
            }
            else {
              $subdomain = $relative[5];
            }

            $matches[6] = $relative[6];
          }

          if ($matches[2] === 'mailto') {
            $subdomain = $matches[2] + ':' + $matches[3] + '@' + $matches[5];
          }

          return {
            host: $subdomain.toLowerCase(),
            tld: $matches[6]
          };
        };

        /**
         * Matches the clicked link's href value with each array of trusted sites.
         *
         * @param {string} url - URL of link clicked.  Used to match any item in trusted site array.
         * @return {Boolean} isTrusted - If set to true, the clicked link is trusted.
       */
        var matchURL = function (url) {

          var whitelist = [];

          // Adds current site's URL to trusted site array.
          whitelist.push(parseURL(window.location.href));

          if ($trusted_sites !== '') {
            for (var i = 0; i < $trusted_sites.length; i++) {
              whitelist.push(parseURL($trusted_sites[i]));
            }
          }

          var urlSegments = parseURL(url);
          var isTrusted = false;

          if (!urlSegments) {
            return true;
          }

          $.each(whitelist, function (i, item) {
            var hostMatches = false;
            var tldMatches = false;

            if ($.trim(item.host) === '*' || $.trim(item.host) === urlSegments.host) {
              hostMatches = true;
            }

            if ($.trim(item.tld) === '*' || $.trim(item.tld) === urlSegments.tld) {
              tldMatches = true;
            }

            if (tldMatches && hostMatches) {
              isTrusted = true;
            }
          });

          return isTrusted;
        };

        /**
         * Sets up countdown timer for each Accessible Exit Disclaimer window.
         *
         * @param {int} duration - Value determines how long until modal window redirects.
         * @param {string} url - URL to redirect to once countdown timer ends.
        */
        var setCountdown = function (duration, url) {
          var display = $('.aed-time-left');
          var timer = duration;
          clearInterval(countdownTimer);
          var countdownTimer = setInterval(function () {
            var seconds = parseInt(timer % 60, 10);
            seconds = seconds < 10 ? '0' + seconds : seconds;

            display.text(seconds);

            if (--timer < 0) {
              clearInterval(countdownTimer);
              window.open(url, '_self');
            }

            if ($modalCloseOutsideWindow === 1) {
              $('#accessible-exit-disclaimer-overlay').click(function () {
                clearInterval(countdownTimer);
              });
            }

            $('.aed-close, .aed-cancel-button').click(function () {
              clearInterval(countdownTimer);
            });

            $('#accessible-exit-disclaimer').keydown(function (event) {
              // if escape pressed clear timer
              if (event.which === 27) {
                clearInterval(countdownTimer);
              }
            });
          }, 1000);
        };

        /**
         * Closes the modal window when the escape key is pressed.
         *
         * Keeps the user inside the modal window when using the tab key.
         *
         * @param {int} obj - HTML content of the disclaimer window.
         * @param {string} evt - Key code of the key pressed.
        */
        var trapKeys = function (obj, evt) {

          var o;

          // if escape pressed
          if (evt.which === 27) {

            // get list of all children elements in given object
            o = obj.find('*');

            // get list of focusable items
            var cancelElement;
            cancelElement = o.filter('.aed-close');

            // close the modal window
            cancelElement.click();
            evt.preventDefault();
          }

          // if tab or shift-tab pressed
          if (evt.which === 9) {

            // get list of all children elements in given object
            o = obj.find('*');

            // get list of focusable items
            var focusableItems = o.filter($focusableElementsString).filter(':visible');

            // get currently focused item
            var focusedItem = $(':focus');

            // get the number of focusable itemsnumberOfFocusableItems;
            var numberOfFocusableItems = focusableItems.length;

            // get the index of the currently focused item
            var focusedItemIndex = focusableItems.index(focusedItem);

            if (evt.shiftKey) {
              // back tab
              // if focused on first item and user preses back-tab, go to the last focusable item
              if (focusedItemIndex === 0) {
                focusableItems.get(numberOfFocusableItems - 1).focus();
                evt.preventDefault();
              }
            }
            else {
              // forward tab
              // if focused on the last item and user preses tab, go to the first focusable item
              if (focusedItemIndex === numberOfFocusableItems - 1) {
                focusableItems.get(0).focus();
                evt.preventDefault();
              }
            }
          }
        };

        /**
         * Builds the Disclaimer and includes options based on Drupal settings.
         *
         * @param {string} url - href attribute of clicked url.
         * @param {object} callback - finds first focusable item.
        */
        var buildDisclaimer = function (url, callback) {
          var cb = callback || function () {};
          var $continueButton = $('.aed-continue-button', '#accessible-exit-disclaimer');
          var $modal = $('#accessible-exit-disclaimer');

          showDisclaimer($modal);

          $modal.on('#accessible-exit-disclaimer', function () {
            cb();
          });

          $continueButton.unbind();
          $continueButton.click(function () {
            window.location.href = url;
          });

          // Displays the countdown redirect timer when enabled in Drupal settings.
          if ($modalCountdownRedirect === 1) {
            setCountdown($modalCountdownTime, url);
          }
        };

        /**
         * Displays modal window and adds ARIA attributes to any HTML element that's
         * not the disclaimer modal window.
         *
         * @param {obj} obj - html contained within modal window.
        */
        var showDisclaimer = function (obj) {
          $($modalBindingElement).attr('aria-hidden', 'true'); // mark the designated selector as hidden.
          $('body > *').attr('aria-hidden', 'true'); // Mark all child elements of body as hidden.
          if ($('div').hasClass('region-page-bottom')) {
            $('.region-page-bottom').attr('aria-hidden', 'false');
          }

          $('#accessible-exit-disclaimer-overlay').css('display', 'block'); // insert an overlay to prevent clicking and make a visual change to indicate the main apge is not available
          $('#accessible-exit-disclaimer').css('display', 'block'); // make the modal window visible
          $('#accessible-exit-disclaimer').attr('aria-hidden', 'false'); // mark the modal window as visible

          // save current focus
          $focusedElementBeforeModal = $(':focus');

          // get list of all children elements in given object
          var o = obj.find('*');

          // set the focus to the first keyboard focusable item
          o.filter($focusableElementsString).filter(':visible').first().focus();
        };

        /**
         * Closes modal window and removes ARIA attributes to any HTML element that's
         * not the disclaimer modal window.
         *
        */
        var hideDisclaimer = function () {
          $('#accessible-exit-disclaimer-overlay').css('display', 'none'); // remove the overlay in order to make the main screen available again
          $('#accessible-exit-disclaimer').css('display', 'none'); // hide the modal window
          $('#accessible-exit-disclaimer').attr('aria-hidden', 'true'); // mark the modal window as hidden
          $($modalBindingElement).attr('aria-hidden', 'false'); // mark the main page as visible
          $('body > *').attr('aria-hidden', 'false'); // Mark all child elements of body as visible.

          // set focus back to element that had it before the modal was opened
          $focusedElementBeforeModal.focus();
        };

        $($anchorLookup).on('click', 'a', function (e) {
          var $link = $(this);
          var url = $link.attr('href');
          var targetAttr = $link.attr('target');

          if (!matchURL(url) && !targetAttr) {
            e.preventDefault();
            buildDisclaimer(url, function () {
              $link.focus();
            });
          }
        });

        $('.aed-close, .aed-cancel-button').click(function (e) {
          hideDisclaimer();
        });

        $('#accessible-exit-disclaimer').keydown(function (event) {
          trapKeys($(this), event);
        });

        // Closes modal window when clicking overlay div. Enabled by "Click to Close"
        // option in the Drupal admin settings.
        if ($modalCloseOutsideWindow === 1) {
          $('#accessible-exit-disclaimer-overlay').click(function (e) {
            hideDisclaimer();
          });
        }
      }
    }
  };
})(jQuery, Drupal);
