# Accessible Exit Disclaimer Module


## Contents of this File

 * Introduction
 * Requirements
 * Installation
 * Configuration
   * Change the Disclaimer Title
   * Change the Disclaimer Message
   * Enter Trusted Sites URLs
   * Path to Custom CSS File
   * Exit Button Image
   * Exit Button Image Alt Attribute
   * Change the "Continue Button" Text and Value attribute
   * Change the "Stay Here" Button Text and Value attribute
   * Assign the Modal Window's Binding HTML Class or ID
   * Assign which HTML element, class, or ID to check for trusted links
   * Change the Offscreen Screen Reader Message
   * Enable Click Outside Modal Window to Close
   * Enable Countdown Redirect
   * Countdown Time
 * CSS Styling
 * Troubleshooting
 * Credits


## Introduction

Currently maintained by: Alek Snyder (alsnyder)

Created by: Alek Snyder (alsnyder)
  [https://www.drupal.org/u/alsnyder](https://www.drupal.org/u/alsnyder) and
  [snyder_alek@bah.com](mailto:snyder_alek@bah.com)

To submit bug reports or feature suggestions,
  [submit a ticket]
  (https://www.drupal.org/project/issues/accessible_exit_disclaimer).

The Accessible Exit Discalimer module displays a modal window for links not
included as a trusted site.  Additionally, the modal window provides numerous
options to customize it's appearance and functionality.


## Requirements

* jQuery 1.7+
* Page bottom region in your template


## Installation

1. Navigate to your preferred module directory (For instance,
    your_drupal_path/sites/all/modules).

2. Download the module from drupal.org.  For more information, see here:
    https://www.drupal.org/docs/7/extending-drupal/installing-modules

3. Enable the module.

4. Configure the module settings from the admin toolbar by navigating to
Configuration -> User Interface -> Accessible Exit Disclaimer or
admin/config/user-interface/accessible-exit-disclaimer.


## Configuration

The module allows for a number of the modal window structure to be edited or
changed in the settings form
(admin/config/user-interface/accessible-exit-disclaimer) form.  This includes
changes to text, CSS, image paths, ARIA attributes, et cetera.

The following instructions for the two fields at the top of the admin settings
form.

**Change the disclaimer title**

Inside the Disclaimer Title field, enter the desired title for your site.  Doing
so, will add your desired title into the Heading 2 tag in the modal window's
header.  For instance, entering "Leaving My Site" would result in a Heading 2
title of "Leaving My Site" at the top of your disclaimer window.

**Change the disclaimer message**

In order to change the Disclaimer Message, enter the message you wish to display
to users.  Styling of any content that is not text is the responsibility of the
site owner. This module was not coded with images, forms, and other HTML
elements in mind. If you want to show content in a certain way, add a custom
stylesheet using the module settings in the "CSS AND LAYOUT" section.

**Enter trusted site URLs**

For each trusted site, enter the full URL and hit enter.  Only one URL is
permitted per line. Otherwise the URLs will not be interpreted by the module
properly. You can enter as many URLs as you'd like.  Only hostnames and TLDs
such as .com are recognized by the module.  Paths and file types are not
supported yet.

**Enter path to custom CSS file**

Adding a CSS stylesheet to the module for styling purposes is really easy.
All you have to do is enter the path to the stylesheet inside the
"Path to Custom CSS File" field.  Please make sure the path ends in ".css"
otherwise you'll receive an error disallowing you from submitting the settings
form.

**Change the exit button image source**

Since this module does its best to keep accessiblity in mind, the exit button is
 an image.  To change the exit button image, enter the path in the "Path to
Exit Button Image" field inside the "Exit Button" fieldset.  PNG images are the
preferred format.

**Change the alt attribute of the exit button image**

In addition to changing the Exit Button image, you can also change the alt
attribute of the exit button.  Enter whatever value you wish to have for the
exit button's alt attribute.  This settings will be located in the "Exit
Button Image Alt Attribute" inside the "Exit Button" fieldset.

**Change the "Continue Button" Text and Value attribute**

Replace the button text for the "Continue" button by entering the desired
text in the "Change Continue Button Text" field inside the "Footer Buttons"
fieldset.

**Change the "Stay Here" Button Text and Value attribute**

Replace the button text for the "Stay Here" button by entering the desired
text inside the "Change Stay Here Button Text" field inside the "Footer Buttons"
fieldset.

**Assign the Modal Window's Binding HTML Class or ID**

Enter a class or ID you wish to bind the modal window in the
"Binding HTML Class or ID" field inside the "CSS and Layout" and "Advanced CSS
and Layout" fieldset. This class or ID, will determine where the modal window
will attach itself to the document.  It's recommended the binding element be a
direct descendant of the body tag.

**Assign which HTML element, class, or ID to check for trusted links**

Enter the HTML element, class, or ID you would like the script to check whether
or not the link is trusted in the "HTML Element for Link Lookup" field inside the
"CSS and Layout" and "Advanced CSS and Layout" fieldset. Keep in mind, only
links within the entered HTML element, class, or ID entered can trigger the
disclaimer. Default is the "body" element.

**Change the offscreen screen reader message**

This setting controls what instructions the modal window gives to screen
reader users.  Recommended values include a detailed description of what the
modal window does when clicked and how to use the modal window.  This setting
can be changed by entering a value inside the "Offscreen Screen Reader Message"
field.

**Enable click outside modal window to close**

Allows for site visitors to close the modal window when clicking outside the
modal window.

**Enable Countdown Redirect**

Enabling this setting allows the modal window to add a redirect countdown to the
page.  When the countdown reaches 0, the page will redirect to the href value of
the link clicked.  Keep in mind this option is not 508 compliant because the
countdown timer will be read over the screen reader message from the module.

**Countdown Time**

If "Enable Countdown Redirect" is enabled, you'll see a textfield to enter the
number of seconds until the page redirects.  Enter any number of seconds
between 0 and 60.


## Troubleshooting

If the Accessible Exit Disclaimer does not display, check the following:

  * Do you have two instances of jQuery being called on the page?

  * Does html.tpl.php of your theme output the $page_bottom variable?


## Credits

The Accessible Exit Disclaimer module would not be possible without seeing the
exit disclaimer [Rescue](http://rescueagency.com/) implemented on [Fresh Empire]
(https://freshempire.betobaccofree.hhs.gov/).  Athough the code is now different
 from their original implementation, the inspiration still remains.  I'd also
 like to thank the contributions of the FCB developers and Booz Allen Developers
 who've provided patches between releases.

This project has been sponsored by:


#### Booz Allen (on behalf of HHS)

![Booz Allen](img/bah_logo.png)

Booz Allen is a recognized leader in Mobile and Digital Strategy, Web Content Management and Cloud Computing, shaping the next generation of technology for the federal government. We help our customers exceed the expectations of the Digital Government Strategy by delivering transformative solutions that emphasize efficiency and mobility, and significantly enhance citizen engagement. Visit [https://www.boozallen.com/expertise/digital-solutions.html](https://www.boozallen.com/expertise/digital-solutions.html) for more information.


#### HHS

It is the mission of the U.S. Department of Health & Human Services (HHS) to
enhance and protect the health and well-being of all Americans. We fulfill that
mission by providing for effective health and human services and fostering
advances in medicine, public health, and social services.  Visit
[hhs.gov](https://www.hhs.gov/) for more information.
